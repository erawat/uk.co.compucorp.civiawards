# CiviAwards

## Is CiviAwards for me?
The CiviAwards extension depends on (uk.co.compucorp.civicase) extension and provides functionality for creating Awards/Grants, managing Applications
and processes.

## How do I get CiviAwards?
CiviAwards is designed to work with CiviCRM 5.x plus. If you are on an earlier version of CiviCRM, you will need to upgrade your site first or contact info@compucorp.co.uk if you needs assistance to do so.

If your CiviCRM is already on CiviCRM 5.x plus and this is the first time you use an extension,  please see [Here](http://wiki.civicrm.org/confluence/display/CRMDOC/Extensions "CiviCRM Extensions Installation") for full instructions and information on how to set and configure extensions.

You can get the latest release of CiviAwards from [Github repository release page](https://github.com/compucorp/uk.co.compucorp.civiawards/releases).

## Support
Please contact the follow email if you have any question: <hello@compucorp.co.uk>

Paid support for this extension is available, please contact us either via Github or at <hello@compucorp.co.uk>